<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Api\ProductController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdministratorController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','role']);
    }



    public function dashboard()
    {
       $products =  ProductController::show();

        return view('administrator.dashboard',compact('products'));
    }

}
