<?php

namespace App\Http\Controllers\Api;
use App\Models\Player;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use Auth;


class PlayerController extends Controller{

    public function __construct()
    {
        $this->middleware(['auth:api']);
    }

    public function index()
    {
        //
    }


    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => ['required', 'string', 'max:150'],
            'apellido' => ['required', 'string', 'max:150'],
            'posicion' => ['required', 'string', 'max:150'],
            'edad' => ['required', 'numeric', 'min:1'],
            'correo' => ['required',  'string', 'email', 'max:255',],
            'descripcion' => ['required'],
            'imagen' => ['required'],
            'equipo_id' => ['required', 'integer'],

        ]);
        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }

        $player = Player::create([
            'name'        => $request->nombre,
            'last_name'   => $request->apellido,
            'positions'   => $request->posicion,
            'age'         => $request->edad,
            'email'       => $request->correo,
            'description' => $request->descripcion,
            'image'       => asset('storage/') . '/' . $request->file('imagen')->store('players_img'),
            'team_id'     => $request->equipo_id
        ]);


            if ($player) {

                return Response::json(array('success' => ['player' => $player]), 200);

            } else {
                return Response::json(array('error' => "Fallo al registrar intentalo mas tarde"), 400);
            }

    }


    public function store(Request $request)
    {
        //
    }


    public function show(Player $player)
    {
        //
    }


    public function edit(Player $player)
    {
        //
    }


    public function update(Request $request, Player $player)
    {
        //
    }


    public function destroy(Player $player)
    {
        //
    }
}
