<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Team;
use App\Models\contactMessage;

use Auth;

class TeamController extends Controller
{


    public function __construct(){
        $this->middleware(['auth:api'])
            ->except(['register','contactUs']);
    }


    public function index()
    {

        $teams = Auth::user()->teams;

        foreach ($teams as $team) {
            $team->players;
        }


        return Response::json(array('success' => ['teams' => $teams]), 200);

    }


    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => ['required', 'string', 'max:255'],
            'nombre_equipo' => ['required', 'string', 'max:255'],
            'correo' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
            'contraseña' => ['required', 'min:6'],
            'telefono' => ['required', 'min:10',  'max:10'],
            'confirmar_contraseña' => ['required', 'min:6', 'same:contraseña'],

        ]);
        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }

        $user = User::create([
            'name' => $request->nombre,
            'email' => $request->correo,
            'password' => Hash::make($request->contraseña),
        ]);


        if ($user) {

            $team = Team::create([
                'team_name' => $request->nombre_equipo,
                'user_id' => $user->id,
                'name_dt' => $user->name,
                'cellphone' => $request->input('telefono')
            ]);
            if ($team) {


                $token = $user->createToken('Personal Access Client')->accessToken;


                $user->teams;
                return Response::json(array('success' => ['user' => $user, 'api_token' => $token]), 200);

            } else {
                return Response::json(array('error' => "Fallo al registrar intentalo mas tarde"), 400);
            }


        } else {
            return Response::json(array('error' => "Fallo al registrar intentalo mas tarde"), 400);
        }
    }

    public function contactUs(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => ['required', 'string', 'max:255'],
            'apellido' => ['required', 'string', 'max:255'],
            'correo' => ['required', 'string', 'email', 'max:255'],
            'mensaje' => ['required', 'min:6']

        ]);
        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }

        $message = contactMessage::create([
            'name' => $request->nombre,
            'last_name' => $request->apellido,
            'email' => $request->correo,
            'message' => $request->mensaje,
        ]);


        if ($message) {
            return Response::json(array('success' => ['user' => $message]), 200);

        } else {
            return Response::json(array('error' => "Fallo al registrar intentalo mas tarde"), 400);
        }

    }


}

