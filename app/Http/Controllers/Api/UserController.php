<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use Auth;

class UserController extends Controller
{


    public function __construct()
    {
        $this->middleware(['auth:api'])
        ->except([
            'login',
            'register'
        ]);
    }


    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'nombre' => ['required', 'string', 'max:255'],
            'correo' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
            'contraseña' => ['required', 'min:6'],
            'confirmar_contraseña' => ['required', 'min:6', 'same:contraseña'],
            'cellphone' => ['required', 'min:10'],

        ]);
        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }

        $user = User::create([
            'name' => $request->nombre,
            'email' => $request->correo,
            'password' => Hash::make($request->contraseña),
            'role' => User::DT
        ]);


        $token = $user->createToken('Personal Access Client')->accessToken;


        if ($user) {

            return Response::json(array('success' => ['user' => $user, 'api_token' => $token]), 200);

        } else {
            return Response::json(array('error' => "Fallo al registrar intentalo mas tarde"), 400);
        }

    }


    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required'],
        ]);
        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }


        $user = User::where('email', Input::get('email'))->first();

        if ($user) {


            if (Hash::check(Input::get('password'), $user->password)) {

                $token = $user->createToken('Personal Access Client')->accessToken;

                return Response::json(array('success' => ['user' => $user, 'api_token' => $token]), 200);
            }
        }

        return Response::json(array('error' => "Usuario/contraseña incorrecta", 'exito' => false), 400);
    }


    public function logout()
    {
        $accessToken =  Auth::user()->token();

        $accessToken->revoke();

        return Response::json(array('success' => "logout"), 200);
    }


    public function AuthInfo()
    {
        return Response::json(array('success' => Auth::user()), 200);
    }


    public function search(Request $request)
    {

        $search = $request->search;

        $users = User::where('nombre', 'like', "%$search%")
            ->Orwhere('nombre', 'like', "%$search%")
            ->Orwhere('correo', 'like', "%$search%")
            ->Orwhere('telefono', 'like', "%$search%")
            ->Orwhere('direccion', 'like', "%$search%")
            ->Orwhere('created_at', 'like', "%$search%")
            ->orderBy('created_at', 'DESC')
            ->paginate(5);

        foreach ($users as $user) {
            $user->direccion = str_limit($user->direccion, 30);

            if (Storage::disk('public')->exists($user->imagen)) {
                $user->imagen = asset('storage') . '/' . $user->imagen;
            } else {
                $user->imagen = asset('img/avatars/default-user.svg');
            }

        }

        return Response::json(array('data' => ['paginate' => ['total' => $users->total(),
            'current_page' => $users->currentPage(),
            'per_page' => $users->perPage(),
            'last_page' => $users->lastPage(),
            'from' => $users->firstItem(),
            'to' => $users->lastPage()],
            'users' => $users,],
            'exito' => true), 200);

    }


    public function update(Request $request, $id)
    {

        $user = User::find($id);


        if ($user) {

            if ($request->has('nombre')) {

                $validator = Validator::make($request->all(), array('nombre' => 'required'));
                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors(), 'exito' => false), 422);
                }

                $user->nombre = $request->nombre;
            }

            if ($request->has('correo')) {

                $validator = Validator::make($request->all(), array('correo' => 'required|email',));
                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors(), 'exito' => false), 422);
                }

                $user->correo = $request->correo;
            }

            if ($request->has('teléfono')) {
                $validator = Validator::make($request->all(), array('teléfono' => 'required'));

                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors(), 'exito' => false), 422);
                }

                $user->telefono = $request->teléfono;
            }

            if ($request->has('dirección')) {
                $validator = Validator::make($request->all(), array('dirección' => 'required'));

                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors(), 'exito' => false), 422);
                }

                $user->direccion = $request->dirección;
            }

            if ($request->has(['contraseña', 'confirmar_contraseña'])) {

                $validator = Validator::make($request->all(), array(
                    'contraseña' => 'required',
                    'confirmar_contraseña' => 'required'));
                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors(), 'exito' => false), 422);
                }

                if ($request->contraseña === $request->confirmar_contraseña) {

                    $user->contraseña = Hash::make($request->contraseña);
                } else {
                    return Response::json(array(
                        'error' => "La nueva contraseña no coinside",
                        'exito' => false), 400);
                }
            }

            if ($request->has('imagen')) {

                if (Storage::disk('public')->exists($user->imagen))
                    Storage::disk('public')->delete($user->imagen);

                $user->imagen = $request->file('imagen')->store('Users_img');

            }

            $user->save();


            return Response::json(array('data' => User::getPathImage($user), 'exito' => true), 200);


        } else {
            return Response::json(array(
                'error' => "User no encontrado",
                'exito' => false), 400);

        }
    }


    public function delete($user_id)
    {

        $user = User::find($user_id);
        if ($user) {

            if (Storage::disk('public')->exists($user->image))
                Storage::disk('public')->delete($user->image);


            $userTokens = $user->tokens;

            foreach ($userTokens as $token) {
                $token->revoke();
            }


            $user->delete();
            return Response::json(array('data' => 'OK'), 200);
        }

        return Response::json(array('error' => "Sin resultados"), 400);

    }


}
