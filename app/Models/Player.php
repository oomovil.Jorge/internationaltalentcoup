<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Player extends Model
{
    protected $fillable = [
        'name', 'last_name', 'positions', 'age', 'email', 'description', 'team_id','image'
    ];


    public static function getPathImage($player)
    {

        if (Storage::disk('public')->exists($player->image)) {
            $player->image = asset('storage') . '/' . $player->image;
        } else {
            $player->image = asset('img/avatars/default-user.svg');
        }

        return $player;


    }

    public function team() {
        return $this->hasOne(Team::class);
    }


}
