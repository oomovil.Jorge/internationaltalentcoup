<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = [
        'team_name', 'user_id', 'name_dt','cellphone'
    ];


    public function players() {
        return $this->hasMany(Player::class);
    }


}
