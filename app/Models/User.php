<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    const DT = 1;
    const SUPERVISOR = 2;
    const MEGASUPERVIDOR = 3;
    const ROOT = 4;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
       'id','password', 'remember_token','role',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function teams()
    {
        return $this->hasMany('App\Models\Team');
    }




}
