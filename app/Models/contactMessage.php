<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class contactMessage extends Model
{
    protected $fillable = [
        'name', 'last_name', 'email','message'
    ];

}
