<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersTable extends Migration
{

    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('last_name');
            $table->string('positions');
            $table->integer('age');
            $table->string('email');
            $table->text('description');
            $table->string('image');
            $table->string('team_id');

            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('players');
    }
}
