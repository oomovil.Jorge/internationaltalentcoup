<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfoliosTable extends Migration
{

    public function up()
    {
        Schema::create('portfolios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('player_id');
            $table->string('type_of_content');
            $table->text('description');
            $table->string('url');
            $table->timestamps();
        });

    }

    public function down()
    {
        Schema::dropIfExists('portfolios');
    }
}
