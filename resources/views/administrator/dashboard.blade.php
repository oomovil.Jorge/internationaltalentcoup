@extends('administrator.layout')

@section('content')

    <div class="container">

        <div class="m-4">
            <passport-authorized-clients></passport-authorized-clients>
        </div>
        <div class="m-4">
            <passport-clients></passport-clients>
        </div>
        <div class="m-4">
            <passport-personal-access-tokens></passport-personal-access-tokens>
        </div>


    </div>
@endsection
