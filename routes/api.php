<?php

use Illuminate\Http\Request;


Route::group(['prefix' => 'user'], function () {

    Route::post('/register', 'Api\TeamController@register');
    Route::post('/login',    'Api\UserController@login');
    Route::get('/AuthInfo',  'Api\UserController@AuthInfo');
    Route::post('/login',    'Api\UserController@login');
    Route::post('/logout',   'Api\UserController@logout');

});


Route::group(['prefix' => 'player'], function () {
    Route::post('/register', 'Api\PlayerController@create');

});

Route::group(['prefix' => 'teams'], function () {
    Route::post('/get', 'Api\TeamController@index');
});


Route::group(['prefix' => 'info'], function () {
    Route::post('/contact/us', 'Api\TeamController@contactUs');
});




