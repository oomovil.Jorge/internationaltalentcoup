<?php

Auth::routes();

Route::get('/', 'Backend\Administrator@index')->name('index');


Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard', 'Administrator\AdministratorController@dashboard')->name('dashboard');
});




